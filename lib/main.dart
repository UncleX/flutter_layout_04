import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
//      title: 'state 状态管理',//没有也可以显示标题
      home: Scaffold(
        appBar: AppBar(
          title: Text('state 状态管理'),
        ),
        body: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                TapBoxA(),
                ParentTapBoxB(),
                ParentTapBoxC()
              ],
            )
          ],
        )
      ),
    );
  }
}

//自己管理
class TapBoxA extends StatefulWidget {
  @override
  _TapBoxAState createState() => _TapBoxAState();
}

class _TapBoxAState extends State<TapBoxA> {
   bool _active = true;
  //方法名首字母小写 小驼峰
  // 否则会报(Name non-constant identifiers using lowerCamelCase" should ignore all-underscore identifiers )警告
  //方法名下回有下划线(曲线)
  void _tapBoxAClicked() {
    setState(() {
      _active = !_active;
    });
  }

  @override
  Widget build(BuildContext context) {
    //有点击手势的控件 用手势包裹
    return GestureDetector(
      //手势点击事件
      onTap: _tapBoxAClicked,
      child: Container(
        child: Center(
          child: Container(
            child: Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(_active ? 'active' : 'unActive',
                      style: TextStyle(fontSize: 12, color: Colors.yellow)),
                  Text('自己管理',
                      style: TextStyle(fontSize: 12, color: Colors.yellow)),
//错误示例:创建了IconButton 一定不能传入null,否则会无限大,超出边界报错.
//                  IconButton(icon: null, onPressed: null)
                ],
              ),
            ),
          )
        ),
        width: 150,
        height: 150,
        //设置背景 /圆角 /阴影 都用此设置
        decoration: BoxDecoration(
          color: _active ? Colors.green : Colors.grey,
        ),
      ),
    );
  }
}

//父控件管理
class ParentTapBoxB extends StatefulWidget {
  @override
  _ParentTapBoxBState createState() => _ParentTapBoxBState();
}

class _ParentTapBoxBState extends State<ParentTapBoxB> {
  bool _active = true;
  void _parentTapBoxClicked(bool newValue){
    setState(() {
      _active = newValue;
    });

  }
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(0.0),
      child: TapBoxB(
        active:_active,
        onChanged: _parentTapBoxClicked,
      ),
    );
  }
}



class TapBoxB extends StatelessWidget {
  //构造方法
  //要使用@required 必须引入import 'package:flutter/foundation.dart';
  TapBoxB({Key key,this.active:false,@required this.onChanged}):super(key:key);

  //公有属性 如下:构造方法里面的属性总是被标记为final
  // This class is the configuration for the state. It holds the values provided by the parent  and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final bool active;
  final ValueChanged<bool> onChanged;

  /*私有方法*/
  void _tapBoxBClicked(){
    onChanged(!active);
  }



  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: _tapBoxBClicked,
        child:Container(
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(active ? 'Active' :'InActive',
                style: TextStyle(
                  color: Colors.yellow,
                  fontSize: 32,
                )),
                Text('父控件管理',
                  style: TextStyle(
                    color: Colors.yellow,
                    fontSize: 12,)
                )
              ],
            ),
          ),
          width: 150,
          height: 150,
          decoration: BoxDecoration(
            color: active ? Colors.green :Colors.grey,
            borderRadius: BorderRadius.all(Radius.circular(10)),
            border: Border.all(color: Colors.red, width: 3.0,),
          ),
        ),
    );
  }
}

//父控件和子控件共同管理

class ParentTapBoxC extends StatefulWidget {
  @override
  _ParentTapBoxCState createState() => _ParentTapBoxCState();
}

class _ParentTapBoxCState extends State<ParentTapBoxC> {
  bool _active = false;

  void _parentHandelTap(bool newValue){
    setState(() {
      print('点击了父控件$widget');
      _active = newValue;
    });
  }


  @override
  Widget build(BuildContext context) {
    return Container(
      child: TabBoxC(
        active: _active,
        onHandleTap: _parentHandelTap,
      ),
    );
  }
}


class TabBoxC extends StatefulWidget {
  TabBoxC({Key key,this.active:false,@required this.onHandleTap});
  //父控件管理按钮状态
  //处理回调(一些潜在的值发生变化)的方法用ValueChanged<T> 相当于接口,回调的定义
  //typedef ValueChanged<T> = void Function(T value);
  final bool active;
  final ValueChanged<bool>onHandleTap;

  //这里不需要 @override 了
//  @override
  _TabBoxCState createState() => _TabBoxCState();
}

class _TabBoxCState extends State<TabBoxC> {



  //自己管理是否边框高亮
  bool _highlight = false;

  //按下
  void _handelTapDown(TapDownDetails tabDownDetails){
    setState(() {
      _highlight = true;
    });
  }

  //抬起
  void _handelTapUp(TapUpDetails tapUpDetails){
    setState(() {
      _highlight = false;
    });
  }
  //取消点击 (方法名里面的单词写错会有波浪线)
  void _handelTapCancel(){
    setState(() {
      _highlight  = false;
    });
  }

 void _handleTap(){
    setState(() {
     print('点击了控件:$widget--$this');
     widget.onHandleTap(!widget.active);
    });
 }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      //边框的实现三种点击事件
      onTapDown: _handelTapDown,
      onTapUp: _handelTapUp,
      onTapCancel: _handelTapCancel,
      onTap: _handleTap,

      child:Container(
      width: 150,
      height: 150,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            child: Text(widget.active?'active':'inActive',
            style: TextStyle(
              color: Colors.yellow,
              fontSize: 12,
            ),),
          ),
          Container(
            child: Text('混合管理',style: TextStyle(color: Colors.yellow,fontSize: 12
            )),
          )
        ],
      ),
      //背景色和边框
      decoration: BoxDecoration(
        color: widget.active?Colors.green:Colors.grey,
        borderRadius: BorderRadius.all(Radius.circular(10.0)),
        //直接控制border的有无
        border: _highlight ? Border.all(width: 3,color: Colors.yellow):null,
      ),

    ),
    );
  }
}




